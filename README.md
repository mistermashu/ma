General
=======

ma stands for make assistant. It automates c++ project builds and provides some script hooks to do more complex stuff.


Common Commands
===============

Compile
-------
	ma

- searches for .ma file and compiles from there.
- it uses the "unity build" pattern to get really fast compile times


Compile and Run
---------------
	ma run

- runs with a cwd of where your .ma is


Compile and Run with DEBUG defined as 1 and with debug flags
----------------------------------
	ma debug


Delete all Generated Files
--------------------------
	ma clean


Show Help Menu
--------------
	ma help


Run Tests
---------------
	ma test
- this defines the TEST macro, builds, and runs the executable.
- this allows you to easily define unit tests into your classes and easily run them.


Install
-------------
	ma install
- this builds the executable like normal, then prompts the user if they want it copied to /usr/local/bin


Setting up Tests
----------------
1. make test functions in your classes, surrounded by #ifdef TEST
2. make an alternate int main() surrounded by #ifdef TEST that calls all of your test functions.
3. the exit status of your test int main() should be how many tests failed. (e.g. if everything works, return 0.)
4. typically, your testing functions should return an int, and you should just return the sum of all of those.


TODO
====

- be able to do additional commands before and after build, before and after run
- check timestamps on all object files vs. source files to automatically determine whether an object file compile is needed
- instead of failing due to /src and /bin dirs not existing, prompt to create them
- make a special .ma directive to check the existence of libraries before trying to compile (with a nice error message if the lib doesn't exist)
- add another command "ma make" that makes a makefile that works similarily to the ma build pattern
- don't do any more compilations if one of them throws an error