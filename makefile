CPPFILES=$(shell find -f src/*.cpp)
HPPFILES=$(shell find -f src/*.hpp)

# builds a local version that you use to test
ma: $(CPPFILES) $(HPPFILES)
	g++ $(CPPFILES) -o ma
clean:
	rm -f ma
debug: $(CPPFILES) $(HPPFILES)
	g++ $(CPPFILES) -g -o ma


# installs ma on your machine so you can run it from within your projects easily
install: ma
	cp ./ma /usr/local/bin
uninstall:
	rm /usr/local/bin/ma
